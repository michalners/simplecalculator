package com.storeware.calculator2;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;


public class LineParserTest {

	@Test
	public void oneAndMoreWhitespace() {
		List<String> lines = Arrays.asList(new String[] {
			"ADD 1",
			"ADD  1",
			"ADD	1",
			"ADD	 1"
		});

		for (String line: lines) {
			LineParser lineParser = new LineParser(line);
			assertEquals("ADD", lineParser.getOperationName());
			assertEquals(1, lineParser.getNumber());
		}
	}
}
