package com.storeware.calculator2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.storeware.calculator2.operations.Substract;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class SubstractTest {

	@Test
	@Parameters({
        "4, 2, 2",
        "-9, 3, -12",
        "-2, -2, 0",
        "1, 2, -1",
	})
	public void doCorectOperation(long x, long y, long result) {
		assertEquals(result, new Substract().calculate(x, y));
	}



}
