package com.storeware.calculator2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.storeware.calculator2.operations.Multiply;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MultiplyTest {

		@Test
		@Parameters({
	        "4, 2, 8",
	        "-9, 3, -27",
	        "-2, -2, 4",
	        "2, -2, -4",
		})
		public void doCorectOperation(long x, long y, long result) {
			assertEquals(result, new Multiply().calculate(x, y));
		}
}
