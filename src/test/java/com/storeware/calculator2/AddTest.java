package com.storeware.calculator2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.storeware.calculator2.operations.Add;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class AddTest {

	@Test
	@Parameters({
        "1, 2, 3",
        "-1, 1, 0",
        "0, 0, 0",
        "-10, -2, -12"
	})
	public void doCorectOperation(long x, long y, long result) {
		Add add = new Add();
		assertEquals(result, add.calculate(x, y));
	}

}
