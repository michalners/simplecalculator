package com.storeware.calculator2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.storeware.calculator2.operations.Divide;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DivideTest {

	@Test
	@Parameters({
        "2, 2, 1",
        "1, 3, 0",
        "6, 3, 2",
        "9, 10, 0",
	})
	public void doCorectOperation(long x, long y, long result) {
		assertEquals(result, new Divide().calculate(x, y));
	}

}
