package com.storeware.calculator2.operations;

public class OperationFactory {
	public static Operation getOperation(String operationName) {
		switch(operationName) {
			case "ADD":
				return new Add();
			case "SUBSTRACT":
				return new Substract();
			case "MULTIPLY":
				return new Multiply();
			case "DIVIDE":
				return new Divide();
			default:
				throw new IllegalArgumentException("Unknown operation: " + operationName);
		}
	}
}
