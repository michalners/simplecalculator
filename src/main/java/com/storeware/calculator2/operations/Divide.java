package com.storeware.calculator2.operations;

public class Divide implements Operation {

	@Override
	public long calculate(long x, long y) {
		return Math.round(x / y);
	}

}
