package com.storeware.calculator2.operations;

public class Multiply implements Operation {

	@Override
	public long calculate(long x, long y) {
		return x * y;
	}

}
