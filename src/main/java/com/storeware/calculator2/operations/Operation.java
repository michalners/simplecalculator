package com.storeware.calculator2.operations;

public interface Operation {
	long calculate(long x, long y);
}
