package com.storeware.calculator2.operations;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Add implements Operation {

	@Override
	public long calculate(long x, long y) {
		return x + y;
	}

}
