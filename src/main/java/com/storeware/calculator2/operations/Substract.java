package com.storeware.calculator2.operations;

public class Substract implements Operation {

	@Override
	public long calculate(long x, long y) {
		return x - y;
	}

}
