package com.storeware.calculator2;

import com.storeware.calculator2.operations.Operation;
import com.storeware.calculator2.operations.OperationFactory;

public class LineParser {
	private String line;

	public LineParser(String line) {
		this.line = line.trim();
	}

	long getNumber() {
		String numberAsString = line.split("\\s+")[1];
		return Long.valueOf(numberAsString);
	}

	String getOperationName() {
		return line.split("\\s+")[0];
	}

	public Operation getOperation() {
		return OperationFactory.getOperation(getOperationName());
	}
}
