package com.storeware.calculator2;

import java.util.List;

public class Calculator {

	public long calculate(List<String> lines) {
		String applyLine = lines.get(lines.size() - 1);
		long result = new LineParser(applyLine).getNumber();

		for(String line: lines) {
			if(line.startsWith("APPLY"))
				break;
			LineParser lineParser = new LineParser(line);
			result = lineParser.getOperation().calculate(result, lineParser.getNumber());
		}

		return result;
	}
}
