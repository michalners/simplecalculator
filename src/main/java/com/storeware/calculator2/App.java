package com.storeware.calculator2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
    	if(args.length < 1) {
    		System.err.println("You need to pass one parameter");
    		return;
    	}

    	String fileName = args[0];

    	try {
			List<String> fileLines = Files.lines(Paths.get(fileName)).map(String::toUpperCase)
				.collect(Collectors.toList());
			System.out.println("result : " + new Calculator().calculate(fileLines));
		} catch (IOException e) {
			System.err.println("Something went wrong: " + e.getMessage());
		}

    }
}
